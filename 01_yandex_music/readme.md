# Исследование данных сервиса “Яндекс.Музыка” — сравнение пользователей двух городов

Вводный проект для отработки навыков работы с тетрадкой, базовым Python и библиотекой Pandas.

По данным Яндекс.Музыки сравнил поведение и предпочтения пользователей Москвы и Санкт-Петербурга.

## Описание
Сравнение Москвы и Петербурга окружено мифами:
 * Москва — мегаполис, подчинённый жёсткому ритму рабочей недели;
 * Петербург — культурная столица, со своими вкусами.

**Цель исследования** — проверить три гипотезы (без статистического теста):
1. Активность пользователей в Москве и Санкт-Петербурге зависит от дня недели.
2. В понедельник утром в Москве преобладают одни жанры, а в Петербурге — другие. Так же и вечером пятницы преобладают разные жанры — в зависимости от города.
3. Москва и Петербург предпочитают разные жанры музыки. В Москве чаще слушают поп-музыку, в Петербурге — русский рэп.

## Итоги исследования
1. Высокая активность пользователей в Москве приходится на на понедельник и среду, а в Санкт-Петербурге на среду.
2. Музыкальные предпочтения не сильно меняются в течение недели:
   * Пользователи слушают похожую музыку в начале недели и в конце.
   * Разница между Москвой и Петербургом не слишком выражена. В Москве чаще слушают русскую популярную музыку, в Петербурге — джаз.
3. Во вкусах пользователей Москвы и Петербурга больше общего, чем различий.

**Инструменты**: `Python` `Pandas`

_keywords_: `обработка данных` `дубликаты` `пропуски` `логическая индексация` `группировка` `сортировка`
